# Eazy HTML5 Elements
##Description
Eazy HTML5 Elements is a WordPress plugin that displays most HTML elements using a shortcode. 
This allows theme developers to visualize how HTML elements will display. 

Eazy HTML5 Elements includes material copied from or derived from HTML5. Copyright © 2014 W3C® (MIT, ERCIM, Keio, Beihang).

HTML elements are grouped into 5 sections following the HTML5 spec by W3C

**HTML HEADINGS**  
h1, h2, h3, h4, h5, h6

**GROUPING HTML CONTENT**  
p, br, pre, blockquote, ol, ul, li, dl, dt, dd, figure, figcaption, div, main

**TEXT LEVEL SEMANTICS**  
a, em, strong, small, s, cite, q, dfn, abbr, data, time, code, var, samp, kbd, sub, sup, i, b, u, mark, ruby, bdi, bdo, span, br, wbr

**EDITS**  
ins, del

**EMBEDDED CONTENT**  
img, iframe, embed, object, param, video, audio, source, track

**TABLES**  
table, thead, tfoot, tbody, tr, th, td

**FORMS**  
form, input
Input types include: 
Text Field, Password Field, Radio Buttons, Checkbox, Number, Date, Color, Range, Month, Week, Time, Datetime, Datetime Local, E-mail, Telephone Number, URL, File, Select, Datalist, Optgroup, Option, Textarea, Keygen, Progress, Meter, Submit Button

##License
License: GPLv2  
License URI: http://www.gnu.org/licenses/gpl-2.0.html  

##Installation
1. Download the plugin
2. Upload plugin to WordPress installation
3. Activate the plugin
4. Create a page or post and use the shortcode [eazyhtml] to display HTML elements

##Frequently Asked Questions 
**How do I make the HTML elements show up?**  
Use the shortcode [eazyhtml] where you want them to display.

**Where can I find more information on HTML5?**  
W3C. Specifically the HTML5 Specifications at w3.org.

##WordPress Requirements
Requires at least: 2.7  
Tested up to: 4.2.4  

##Changelog
= 1.0 =  
First version


##Screenshots  
HTML heading elements.  
![screenshot-01.jpg](https://bitbucket.org/repo/Bqyzgo/images/1712301685-screenshot-01.jpg)

Elements for grouping HTML content.  
![screenshot-02.jpg](https://bitbucket.org/repo/Bqyzgo/images/2657098864-screenshot-02.jpg)

Elements for HTML text level semantics.  
![screenshot-03.jpg](https://bitbucket.org/repo/Bqyzgo/images/1745029860-screenshot-03.jpg)

Elements for editing HTML content.  
![screenshot-04.jpg](https://bitbucket.org/repo/Bqyzgo/images/1198133292-screenshot-04.jpg)

Elements for embedding HTML content. Files are included in /resources.  
![screenshot-05.jpg](https://bitbucket.org/repo/Bqyzgo/images/1621007967-screenshot-05.jpg)

Elements for displaying HTML tables.  
![screenshot-06.jpg](https://bitbucket.org/repo/Bqyzgo/images/1430700395-screenshot-06.jpg)

Elements for displaying HTML forms.  
![screenshot-07.jpg](https://bitbucket.org/repo/Bqyzgo/images/4041830081-screenshot-07.jpg)
